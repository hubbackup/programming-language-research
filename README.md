<samp>

# Programming Language Research

Research on programming languages, compilers, functional programming, devtools.

## Research

All [notes](./research) from my research on programming languages and compilers.

### Courses & Notes

Tooking classes to learn compiler theory more in-depth. Here's all courses I took and the notes I wrote.

- [Programming Languages Part A](./research/courses/programming-languages-part-a)
- [Compilers: Theory and Practice](./research/courses/compilers-theory-and-practice)
- [Compiler — Stanford](./research/courses/compilers-stanford-course)
- [Introduction to Functional Programming](./research/courses/introduction-to-functional-programming)
- [UFMG Compilers Course](./research/courses/ufmg-compilers-course)

### Books & Notes

- [SICP — Structure and Interpretation of Computer](./research/books/sicp)
- [Modern Compiler Implementation in C](https://www.amazon.com/Modern-Compiler-Implementation-Andrew-Appel/dp/0521607655)
- [Engineering a Compiler](https://www.amazon.com/Engineering-Compiler-Keith-D-Cooper/dp/0128154128/ref=sr_1_1?keywords=Engineering+a+Compiler&qid=1675768541&s=books&sr=1-1)
- [Parsing Techniques. A Practical Guide](https://www.amazon.com/Parsing-Techniques-Practical-Monographs-Computer-ebook/dp/B0017AMLL8/ref=sr_1_1?crid=1W94M9OYZ6MH8&keywords=Parsing+Techniques.+A+Practical+Guide&qid=1675768574&s=books&sprefix=parsing+techniques.+a+practical+guide%2Cstripbooks-intl-ship%2C211&sr=1-1)
- [Modern Compiler Design](https://www.amazon.com/Modern-Compiler-Design-Dick-Grune/dp/1461446988/ref=sr_1_1?crid=7JFBNCBOHIW9&keywords=Modern+Compiler+Design&qid=1675768609&s=books&sprefix=modern+compiler+design%2Cstripbooks-intl-ship%2C228&sr=1-1)
- [The Elements Of Computing Systems](https://www.amazon.com/Elements-Computing-Systems-second-Principles/dp/0262539802/ref=sr_1_1?crid=2CZ8FBOMQ0X7C&keywords=The+Elements+Of+Computing+Systems&qid=1675768639&s=books&sprefix=the+elements+of+computing+systems%2Cstripbooks-intl-ship%2C463&sr=1-1)
- [Parsing Techniques: A Practical Guide](https://www.goodreads.com/en/book/show/1756599)

### Experiments

- [An interpreter for the Monkey programming language](https://github.com/imteekay/monkey-ts)
- [An interpreter for the Lox programming language](./research/experiments/lox.ts)
- [How the TypeScript compiler works](./research/experiments/typescript)

### Papers

- [How Statically-Typed Functional Programmers Write Code](./research/papers/how-statically-typed-functional-programmers-write-code)
- [An Axiomatic Basis for Computer Programming](http://sunnyday.mit.edu/16.355/Hoare-CACM-69.pdf)
- [The Next 700 Programming Languages](https://www.cs.cmu.edu/~crary/819-f09/Landin66.pdf)
- [A Theory of Type Polymorphism in Programming](https://homepages.inf.ed.ac.uk/wadler/papers/papers-we-love/milner-type-polymorphism.pdf)
- [Towards a theory of type structure](https://www.cis.upenn.edu/~stevez/cis670/pdfs/Reynolds74.pdf)
- [Call-by-name, call-by-value, and the λ-calculus](https://www.sciencedirect.com/science/article/pii/0304397575900171?ref=pdf_download&fr=RR-2&rr=795ba8dc2acd4edd)
- [A syntactic approach to type soundness](https://web.eecs.umich.edu/~weimerw/2008-615/reading/wright92syntactic.pdf)
- [Fundamental concepts in programming languages](https://www.math.pku.edu.cn/teachers/qiuzy/plan/lits/FundamentalConceptOfPL.pdf)

## Programming Language Design / Theory

- [A Tale of Two Asyncs: Open Source Language Design in Rust and Node.js](https://www.youtube.com/watch?v=aGJTXdXQN2o)
- [Another Go at Language Design](https://www.youtube.com/watch?v=7VcArS4Wpqk)
- [How Rust does OSS](https://www.youtube.com/watch?v=m0rakUuPXFM)
- [Growing a Language, by Guy Steele](https://www.youtube.com/watch?v=_ahvzDzKdB0)
- [The Mess We're In](https://www.youtube.com/watch?v=lKXe3HUG2l4)
- [A Frontend Programmer's Guide to Languages](https://thatjdanisso.cool/programming-languages)
- [Let’s Build a Programming Language](https://medium.com/hackernoon/lets-build-a-programming-language-2612349105c6)
- [Grammars for programming languages](https://medium.com/@mikhail.barash.mikbar/grammars-for-programming-languages-fae3a72a22c6)
- [Building a Debugger: Code Analysis](https://nan.fyi/debugger)
- [Language-oriented software engineering](https://parametri.city/blog/2018-12-23-language-oriented-software-engineering/index.html)
- [The Programming Language Enthusiast](http://www.pl-enthusiast.net/)
- [Programming and Programming Languages](https://papl.cs.brown.edu/2018/index.html)
- [Programming Language Foundations in Agda](https://plfa.github.io/beta/)
- [Programming Language and Compilers Reading List](https://www.jntrnr.com/programming-language-and-compilers-reading-list/)
- [On understanding types, data abstraction and effects](https://www.youtube.com/watch?v=3Cdw5x72k_g&ab_channel=MicrosoftResearch)
- [What's Next for Our Programming Languages](https://www.youtube.com/watch?v=q3XcQh0cNZM&ab_channel=InfoQ)
- [JavaScript Static Analysis for Evolving Language Specifications](https://www.youtube.com/watch?v=3Jlu_jnHB8g&ab_channel=PLRGKAIST)
- [Minimalism in Programming Language Design](https://pointersgonewild.com/2022/05/23/minimalism-in-programming-language-design)
- [Programming language theory and practice in ReScript](https://bobzhang.github.io/rescript-pl-course)
- [CS520 Theories of Programming Languages — KAIST](https://www.youtube.com/playlist?list=PLvV9DPeJV9xx0NegrG4gNu8WXSXibDICu)
- [Panel: The Future of Programming Languages](https://learn.microsoft.com/en-us/events/pdc-pdc08/tl57)
- [Shriram Krishnamurthi and Joe Gibbs Politz - Programming Languages: Application and Interpretation](http://papl.cs.brown.edu/2015/)
- [Principles of Programming Languages](https://www.youtube.com/playlist?list=PLK06XT3hFPzilgF1mi_hHqcXO1-o_8OEe)

## Compilers

### Compiler Construction

- [Let's build a compiler](https://generalproblem.net/)
- [Compiler Design course](https://www.youtube.com/playlist?list=PLDAE55AEE0731D729&feature=plcp)
- [Lexical Analysis](https://medium.com/hackernoon/lexical-analysis-861b8bfe4cb0)
- [AST (Abstract Syntax Tree)](https://medium.com/@dinis.cruz/ast-abstract-syntax-tree-538aa146c53b)
- [Compilers and Interpreters](https://medium.com/hackernoon/compilers-and-interpreters-3e354a2e41cf)
- [Anders Hejlsberg on Modern Compiler Construction](https://www.youtube.com/watch?v=wSdV1M7n4gQ)
- [Language grammar](https://stackoverflow.com/a/4726662/11314146)
- [Lexing in JS style](https://areknawo.com/the-lexer-in-js/)
- [Mozilla Hacks Compiler Compiler](https://www.youtube.com/playlist?list=PLo3w8EB99pqKF1FQllRsxyQh3sA7V2Hc-)
- [Resources for Amateur Compiler Writers](https://c9x.me/compile/bib/)
- [Understanding compiler optimization](https://www.youtube.com/watch?v=haQ2cijhvhE)
- [An Incremental Approach to Compiler Construction](http://scheme2006.cs.uchicago.edu/11-ghuloum.pdf)
- [Lessons from Writing a Compiler](https://borretti.me/article/lessons-writing-compiler)
- [Understanding GC in JSC From Scratch](https://webkit.org/blog/12967/understanding-gc-in-jsc-from-scratch)
- [JavaScript implementation in SpiderMonkey](https://www.youtube.com/playlist?list=PLo3w8EB99pqJVPhmYbYdInBvAGarDavh-)
- [Let's make a Teeny Tiny compiler, part 1](https://austinhenley.com/blog/teenytinycompiler1.html)
- [Let's make a Teeny Tiny compiler, part 2](https://austinhenley.com/blog/teenytinycompiler2.html)
- [Let's make a Teeny Tiny compiler, part 3](https://austinhenley.com/blog/teenytinycompiler3.html)
- [JavaScript ∩ WebAssembly](https://www.youtube.com/watch?v=PP5r_b3Ws8s&ab_channel=ReactEurope)
- [Jack W. Crenshaw - Let’s Build a Compiler!](http://compilers.iecc.com/crenshaw/tutorfinal.pdf)
- [Douglas Crockford - Top Down Operator Precedence](http://javascript.crockford.com/tdop/tdop.html)
- [Bob Nystrom - Expression Parsing Made Easy](http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy/)
- [Dr. Dobbs - Bob: A Tiny Object-Oriented Language](http://www.drdobbs.com/open-source/bob-a-tiny-object-oriented-language/184409401)
- [Nick Desaulniers - Interpreter, Compiler, JIT](https://nickdesaulniers.github.io/blog/2015/05/25/interpreter-compiler-jit/)
- [Fredrik Lundh - Simple Town-Down Parsing In Python](https://11l-lang.org/archive/simple-top-down-parsing)
- [How to implement a programming language in JavaScript](http://lisperator.net/pltut/)
- [Scheme from Scratch - Introduction](http://peter.michaux.ca/articles/scheme-from-scratch-introduction)
- [Make a Lisp](https://github.com/kanaka/mal)
- [Compiling to lambda-calculus](https://matt.might.net/articles/compiling-up-to-lambda-calculus/)
- [Compiling Scheme to C with closure conversion](https://matt.might.net/articles/compiling-scheme-to-c)
- [Compiling to Java](https://matt.might.net/articles/compiling-to-java)
- [Implementing a bignum calculator](https://www.youtube.com/watch?v=PXoG0WX0r_E)
- [Lexical Scanning in Go](https://www.youtube.com/watch?v=HxaD_trXwRE)
- [Garbage Collection Algorithms](https://www.dmitrysoshnikov.education/p/essentials-of-garbage-collectors)
- [Building a Parser from scratch](https://www.dmitrysoshnikov.education/p/parser-from-scratch)
- [Parsing Algorithms](https://www.dmitrysoshnikov.education/p/essentials-of-parsing)
- [IU Compiler Course](https://iucompilercourse.github.io/IU-P423-P523-E313-E513-Fall-2020)
- [Building a parser in C#, from first principles](https://www.youtube.com/watch?v=klHyc9HQnNQ&ab_channel=NDCConferences)
- [Flattening ASTs (and Other Compiler Data Structures)](https://www.cs.cornell.edu/~asampson/blog/flattening.html)
- [Pursuit of Performance on Building a JavaScript Compiler](https://rustmagazine.org/issue-3/javascript-compiler)
- [How would compilers change in the next 10 years?](https://www.youtube.com/watch?v=p15qRHZ_Leg&ab_channel=CompilerConstruction)
- [How To Build A Programming Language From Scratch](https://www.youtube.com/watch?v=8VB5TY1sIRo&list=PL_2VhOvlMk4UHGqYCLWc6GO8FaPl8fQTh&ab_channel=tylerlaceby)

### Interpreter

- [Building an Interpreter](https://www.iamtk.co/series/building-an-interpreter)
- [Crafting an Interpreter](https://github.com/imteekay/crafting-an-interpreter)
- [Let’s Build A Simple Interpreter](https://ruslanspivak.com/lsbasi-part1/)
- [Crafting Interpreters](http://craftinginterpreters.com/contents.html)
- [Implementing a Simple Compiler on 25 Lines of JavaScript](https://blog.mgechev.com/2017/09/16/developing-simple-interpreter-transpiler-compiler-tutorial/)
- [Little lisp interpreter](https://maryrosecook.com/blog/post/little-lisp-interpreter)
- [Pratt Parsers: Expression Parsing Made Easy](http://journal.stuffwithstuff.com/2011/03/19/pratt-parsers-expression-parsing-made-easy)
- [A Python Interpreter Written in Python](http://aosabook.org/en/500L/a-python-interpreter-written-in-python.html)
- [(How to Write a (Lisp) Interpreter (in Python))](http://norvig.com/lispy.html)
- [Little Lisp interpreter](https://www.recurse.com/blog/21-little-lisp-interpreter)
- [Building an Interpreter from scratch](https://www.dmitrysoshnikov.education/p/essentials-of-interpretation)
- [Cheaply writing a fast interpreter](https://www.youtube.com/watch?v=V8dnIw3amLA&ab_channel=PerformanceSummit)

### Modern Compilers

- [Write you a Haskell](http://dev.stephendiehl.com/fun/)
- [Quick look at the TypeScript Compiler API](https://www.youtube.com/watch?v=AnjfW633e8o%E2%80%8E.%E2%80%8Edev)
- [Typescript Compiler explained by the Author Anders Hejlsberg](https://www.youtube.com/watch?v=f6TCB61fDwY)
- [TypeScript Compiler Notes](https://github.com/microsoft/TypeScript-Compiler-Notes)
- [A miniature model of the Typescript compiler](https://github.com/sandersn/mini-typescript)

### Type System

- [Let's build a typesystem in Haskell!](https://www.youtube.com/watch?v=aZFWY6DxX54)
- [Safe & Efficient Gradual Typing for TypeScript](https://www.cs.umd.edu/~aseem/safets.pdf)
- [A Type System From Scratch](https://www.youtube.com/watch?v=IbjoA5xVUq0)
- [Typing the Untyped: Soundness in Gradual Type Systems](https://www.youtube.com/watch?v=uJHD2xyv7xo)
- ["Propositions as Types" by Philip Wadler](https://www.youtube.com/watch?v=IOiZatlZtGU)
- [Type-safe embedded domain-specific languages 1/4](https://www.youtube.com/watch?v=4sTeT7poU3g)
- [Type-safe embedded domain-specific languages 2/4](https://www.youtube.com/watch?v=OM_riJgZF8A)
- [Type-safe embedded domain-specific languages 3/4](https://www.youtube.com/watch?v=gFJTKJgL2zI)
- [Type-safe embedded domain-specific languages 4/4](https://www.youtube.com/watch?v=isAu8YuI6SA)
- [A Taste of Type Theory](https://www.youtube.com/watch?v=--2jJwdQ5js)
- [An accessible introduction to type theory and implementing a type-checker](https://mukulrathi.com/create-your-own-programming-language/intro-to-type-checking)
- [Discrete Math — Dr. Trefor Bazett](https://www.youtube.com/playlist?list=PLHXZ9OQGMqxersk8fUxiUMSIx0DBqsKZS)
- [Building a Typechecker from scratch](https://www.udemy.com/course/typechecker)
- [Hindley-Milner Type Checking AST](https://adamdoupe.com/teaching/classes/cse340-principles-of-programming-languages-f15/slides/Hindley-MilnerTypeChecking-Mohsen-Zohrevandi.pdf)
- [The Hindley-Milner Type System](https://www.youtube.com/watch?v=OyrByPkiX7s&ab_channel=NicolasLaurent)
- ["Hindley-Milner Type Inference — Part 1](https://www.youtube.com/watch?v=cQf_6NsLp80&ab_channel=AdamDoup%C3%A9)
- ["Hindley-Milner Type Inference — Part 2](https://www.youtube.com/watch?v=xJXcZp2vgLs&list=PLK06XT3hFPzilgF1mi_hHqcXO1-o_8OEe&index=37&ab_channel=AdamDoup%C3%A9)
- [A reckless introduction to Hindley-Milner type inference](https://www.lesswrong.com/posts/vTS8K4NBSi9iyCrPo/a-reckless-introduction-to-hindley-milner-type-inference)

## Virtual Machines (VMs)

- [Building a Virtual Machine](https://www.dmitrysoshnikov.education/p/virtual-machine)

## Books & Papers

- [Practical Foundations for Programming Languages](https://www.amazon.com/Practical-Foundations-Programming-Languages-Professor/dp/1107029570)
- [SICP](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book.html)
- [HTDP](https://htdp.org/2020-8-1/Book/index.html)
- [Papers to read](https://github.com/haskellcamargo/papers-to-read)
- [Papers on programming languages: ideas from 70's for today](https://medium.com/@mikhail.barash.mikbar/papers-on-programming-languages-ideas-from-70s-for-today-2931891d4dbd)

## Language Learnings

- [JavaScript](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/javascript)
- [Python](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/python)
- [Ruby](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/ruby)
- [Haskell](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/haskell)
- [Elixir](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/elixir)
- [Clojure](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/clojure)
- [Rust](https://github.com/imteekay/programming-language-theory/tree/master/language-learning/rust)

### TypeScript

- [Type level programming in TypeScript](https://mjj.io/2021/03/29/type-level-programming-in-typescript)
- [Advanced Types in TypeScript](https://www.youtube.com/playlist?list=PLw5h0DiJ-9PBIgIyd2ZA1CVnJf0BLFJg2)
- [An Introduction To Type Programming In TypeScript](https://www.zhenghao.io/posts/type-programming)
- [Type Level Programming in TypeScript](https://www.youtube.com/watch?v=vGVvJuazs84)
- [Optimizing TypeScript Memory Usage](https://swatinem.de/blog/optimizing-tsc)
- [TypeScript Bytecode Interpreter / Runtime Types](https://github.com/microsoft/TypeScript/issues/47658)
- [Typescript Type System](https://www.youtube.com/watch?v=svQnyVny-Hw&ab_channel=HansSchenker)
- [How the TypeScript Compiler Compiles](https://www.youtube.com/watch?v=X8k_4tZ16qU&ab_channel=ortatherox)
- [Debugging the TypeScript Codebase](https://blog.andrewbran.ch/debugging-the-type-script-codebase)
- [TypeScript AST Resources](https://twitter.com/grow_love/status/1585711764565811201)
- [TypeScript Compiler API: Improve API Integrations Using Code Generation](https://blog.appsignal.com/2021/08/18/improve-api-integrations-using-code-generation.html)
- [Investigating TypeScript compiler APIs](https://blog.scottlogic.com/2015/01/20/typescript-compiler-api.html)
- [A horrifically deep dive into TypeScript module resolution](https://www.youtube.com/watch?v=MEl2R7mEAP8&ab_channel=MichiganTypeScript)
- [TypeScript Compiler Manual](https://sandersn.github.io/manual/Typescript-compiler-implementation.html)
- [Making sense of TypeScript using set theory](https://blog.thoughtspile.tech/2023/01/23/typescript-sets)
- [Reconstructing TypeScript, part 0: intro and background](https://jaked.org/blog/2021-09-07-Reconstructing-TypeScript-part-0)
- [Reconstructing TypeScript, part 1: bidirectional type checking](https://jaked.org/blog/2021-09-15-Reconstructing-TypeScript-part-1)
- [Reconstructing TypeScript, part 2: functions and function calls](https://jaked.org/blog/2021-09-27-Reconstructing-TypeScript-part-2)
- [Reconstructing TypeScript, part 3: operators and singleton types](https://jaked.org/blog/2021-10-06-Reconstructing-TypeScript-part-3)
- [Reconstructing TypeScript, part 4: union types](https://jaked.org/blog/2021-10-14-Reconstructing-TypeScript-part-4)
- [Reconstructing TypeScript, part 5: intersection types](https://jaked.org/blog/2021-10-28-Reconstructing-TypeScript-part-5)
- [Reconstructing TypeScript, part 6: narrowing](https://jaked.org/blog/2021-11-11-Reconstructing-TypeScript-part-6)
- [Static TypeScript](https://www.microsoft.com/en-us/research/uploads/prod/2019/09/mplr19main-id10-p-41a6cf2-42682-final.pdf)

### Ecmascript

- [Automatic semicolon insertions in JavaScript](https://articles.wesionary.team/automatic-semicolon-insertions-in-javascript-best-practices-c49ed8222e12)
- [`SingleEscapeCharacter`](https://262.ecma-international.org/13.0/#prod-SingleEscapeCharacter)
- [Empty Statement](https://tc39.es/ecma262/multipage/ecmascript-language-statements-and-declarations.html#sec-empty-statement)

### Rust

- [Why Static Languages Suffer From Complexity](https://hirrolot.github.io/posts/why-static-languages-suffer-from-complexity)
- [Things I Learned (TIL) - Nicholas Matsakis](https://www.youtube.com/watch?v=LIYkT3p5gTs&ab_channel=PLISS)

## Browser

- [Browser from scratch](https://zerox-dg.github.io/blog/tags/browser-from-scratch/)
- [Let's build a browser engine!](https://limpet.net/mbrubeck/2014/08/08/toy-layout-engine-1.html)
- [How Browsers Work: Behind the scenes of modern web browsers](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/)
- [Concurrent JavaScript: It can work!](https://webkit.org/blog/7846/concurrent-javascript-it-can-work)
- [Web Browser Engineering](https://browser.engineering)
- [Round-up of web browser internals resources](https://developer.chrome.com/blog/round-up-of-web-browser-internals-resources)
- [How browsers work](https://web.dev/howbrowserswork)
- [Browser Performance](https://www.kuniga.me/blog/2020/03/28/browser-performance.html)
- [Notes on how browsers work](https://www.kuniga.me/blog/2015/10/09/notes-on-how-browsers-work.html)
- [Notes on Javascript Memory Profiling in Google Chrome](https://www.kuniga.me/blog/2015/06/07/notes-on-javascript-memory-profiling-in-google-chrome.html)
- [Notes on JavaScript Interpreters](https://www.kuniga.me/blog/2017/06/01/notes-on-javascript-interpreters.html)
- [Web Browser Engineering](https://browser.engineering)
- [Critical rendering path - Crash course on web performance](https://www.youtube.com/watch?v=PkOBnYxqj3k&ab_channel=IlyaGrigorik)
- [Understanding Reflow and Repaint in the browser](https://dev.to/gopal1996/understanding-reflow-and-repaint-in-the-browser-1jbg)
- [In The Loop - setTimeout, micro tasks, requestAnimationFrame, requestIdleCallback](https://www.youtube.com/watch?v=cCOL7MC4Pl0&ab_channel=JSConf)
- [reflows & repaints: css performance making your javascript slow?](http://www.stubbornella.org/content/2009/03/27/reflows-repaints-css-performance-making-your-javascript-slow)
- [What forces layout / reflow](https://gist.github.com/paulirish/5d52fb081b3570c81e3a)
- [Rendering: repaint, reflow/relayout, restyle](https://www.phpied.com/rendering-repaint-reflowrelayout-restyle)
- [David Baron's blog: Blink engineer](https://dbaron.org/log/)
- [The internet computer](https://www.youtube.com/watch?v=v0160IirdL4&ab_channel=TheBrowserCompany)
- [Servo: Designing and Implementing a Parallel Browser](https://www.youtube.com/watch?v=67QP8t-89VM)
- [Building a Rust Web Browser](https://joshondesign.com/tags/browser)
- [Browser from Scratch](https://zerox-dg.github.io/blog/tags/browser-from-scratch)
- [Browser From Scratch Live Streams](https://www.youtube.com/playlist?list=PLgOaDFg2Sml4CG1tYeYhQ6eKqyh0hmyg1)
- [Intro to Rust-lang (Building the Dom and an HTML Parser)](https://www.youtube.com/watch?v=brhuVn91EdY&list=PLJbE2Yu2zumDF6BX6_RdPisRVHgzV02NW&index=22&ab_channel=TensorProgramming)
- [Intro to Rust-lang (Adding a CSS engine and CSS parsing to our Browser)](https://www.youtube.com/watch?v=dnrEto7adg0&list=PLJbE2Yu2zumDF6BX6_RdPisRVHgzV02NW&index=24&ab_channel=TensorProgramming)
- [Intro to Rust-lang (Adding a Style Tree to our Browser)](https://www.youtube.com/watch?v=8e37RsyiGSE&list=PLJbE2Yu2zumDF6BX6_RdPisRVHgzV02NW&index=25&ab_channel=TensorProgramming)
- [Intro to Rust (Building a Browser Engine: Commands and Rendering in OpenGL)](https://www.youtube.com/watch?v=rszgtm7i0n8&list=PLJbE2Yu2zumDF6BX6_RdPisRVHgzV02NW&index=27&ab_channel=TensorProgramming)
- [Chrome University 2018: Life of a Script](https://www.youtube.com/watch?v=voDhHPNMEzg&ab_channel=BlinkOn)

### Web UI

- [PL web frameworks](https://twitter.com/sliminality/status/1516175554550018048)
- [Algebraic Effects for the Rest of Us](https://overreacted.io/algebraic-effects-for-the-rest-of-us/)
- [Algebraic Effects for React Developers](https://blog.reesew.io/algebraic-effects-for-react-developers)
- [Algebraic effects, Fibers, Coroutines](https://www.youtube.com/watch?v=vzzOdWj4YyM&ab_channel=YouGottaLoveFrontend)
- [TypeScript + fp-ts: ReaderTaskEither and React](https://andywhite.xyz/posts/2021-01-28-rte-react/)
- [React - Basic Theoretical Concepts](https://github.com/reactjs/react-basic)
- [React Concurrent mode](https://twitter.com/dan_abramov/status/1120971795425832961)

### Chrome

- [Overview of the RenderingNG architecture](https://developer.chrome.com/articles/renderingng-architecture)
- [Key data structures and their roles in RenderingNG](https://developer.chrome.com/articles/renderingng-data-structures)
- [RenderingNG deep-dive: LayoutNG](https://developer.chrome.com/articles/layoutng)
- [RenderingNG: an architecture that makes and keeps Chrome fast for the long term](https://blog.chromium.org/2021/10/renderingng.html)

## Careers

### Jobs

- [Chromium Engineer at Browser Company](/careers/chromium-engineer-browser-company.pdf)
- [Senior Software Engineer at Mozilla Corporation](/careers/mozilla-firefox-senior-software-engineer.pdf)
- [JavaScript Virtual Machine Compiler Engineer at Apple](/careers/javascript-virtual-machine-compiler-engineer.pdf)
- [Compiler Jobs](https://mgaudet.github.io/CompilerJobs)
- [Swift Type System Engineer](/careers/swift-type-system-engineer-apple.pdf)

### Learning Paths

- [Compiler Engineer Path](http://dmitrysoshnikov.com/courses/compiler-engineer-path)

## People in PL

- [Anders Hejlsberg](https://www.youtube.com/watch?v=2K_4T7M1DKk&ab_channel=AarthiandSriram)
- [Dmitry Soshnikov](https://twitter.com/DmitrySoshnikov)

## License

[MIT](/LICENSE) © [TK](https://iamtk.co)

</samp>
